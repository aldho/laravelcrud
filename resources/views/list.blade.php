<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{URL::to('/resources/assets/css/style.css')}}">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <script>
        $(document).ready(function()
        {
            $(".del_rec").on('click',function (event) {
            var x = confirm("Are you sure you want to delete?");
            if (x) {
                return true;
            }
            else {
                event.preventDefault();
                return false;
            }
            });
        });
        </script>
    </head>
    <body>
        <div class="container">
          
        
          <h1 style="text-align: center;" class="heading">CRUD Laravel </h1>
         
          <h2 style="text-align: center;">Employee Listing</h2>
          <div>
              @if(Session::has('success'))
                  <div class="alert alert-success">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>{!! Session::get('success') !!}</strong>
                  </div>
              @endif
          </div>
          <a href="employee/create" class="btn btn-primary" style="float:right;">Add Employee</a>

          <table class="table">
            <thead>
              <tr>
                <th>Name</th>
                <th>E-mail</th>
                <th>Gender</th>
                <th>Image</th>
                <th>Created On</th>
              </tr>
            </thead>
            <tbody>
               @if(!$employees->isEmpty())
                   @foreach($employees as $emp)
                      <tr class="active">
                        <td>{{$emp->emp_name}}</td>
                        <td>{{$emp->emp_email}}</td>
                        <td>{{$emp->emp_gender=='0' ? 'Male' : 'Female'}}</td>
                        @if($emp->emp_image!='')
                        <td><img src="{{ URL::to('/uploads/'.$emp->emp_image) }}" width="100px" height="50px" class="img-responsive" /></td>
                        @else
                          @if($emp->emp_gender=='0')
                              <td><img src="{{ URL::to('/uploads/male.jpg') }}" width="100px" height="50px" class="img-responsive" /></td>
                          @else
                              <td><img src="{{ URL::to('/uploads/female.jpg') }}" width="100px" height="50px" class="img-responsive" /></td>
                          @endif
                        @endif
                        <td>{{Carbon\Carbon::parse($emp->created_on)->format('d-M-Y')}}</td>
                        <td>
                            <div>
                                <a href="employee/{{ $emp->emp_id }}/edit" class="btn btn-primary btn-edit">Edit</a>

                                <a href="employee/{{ $emp->emp_id }}" class="btn btn-primary btn-view">View</a>

                            {!! Form::open(['url'=>'employee/'.$emp->emp_id,'method' => 'delete','class' => 'view_btn']) !!}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger del_rec')) }}
                            {!! Form::close() !!}

                            </div>
                            
                        </td>
                      </tr>
                     @endforeach
               @else
                    <tr class="active" style="text-align: center;">
                      <td colspan="12">No Record(s) Found</td>    
                    </tr>
              @endif
            </tbody>
          </table>
          {{ $employees->links() }}
        </div>
    </body>
</html>
