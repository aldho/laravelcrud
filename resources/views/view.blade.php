<!DOCTYPE html>
<html lang="en">
<head>
  <title>View Employee</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="{{URL::to('/resources/assets/css/style.css')}}">
</head>
<body>
    <div class="container">
      <h2 style="text-align: center;">View Employee</h2>

      <table class="table">
            <tbody>
                <tr class="active"><td><strong>Name</strong> </td><td>{{$employee->emp_name}}</td></tr>
                <tr class="info"><td><strong>Email </strong></td><td>{{$employee->emp_email}}</td></tr>
                <tr class="active"><td><strong>Gender </strong> </td><td>{{$employee->emp_gender=='0' ? 'Male' : 'Female'}}</td></tr>
                <tr class="info"><td><strong> Address </strong> </td>
                  <td>{{$employee->emp_address}}</td>
                </tr>
                <tr class="active"><td><strong> Image </strong> </td>
                  @if($employee->emp_image!='')
                  <td><img src="{{ URL::to('/uploads/'.$employee->emp_image) }}" width="100px" height="50px" class="img-responsive" /></td>
                  @else
                    @if($employee->emp_gender=='0')
                        <td><img src="{{ URL::to('/uploads/male.jpg') }}" width="100px" height="50px" class="img-responsive" /></td>
                    @else
                        <td><img src="{{ URL::to('/uploads/female.jpg') }}" width="100px" height="50px" class="img-responsive" /></td>
                    @endif
                  @endif
                </tr>
            </tbody>
          </table>
    <div class="form-group">        
      <div class="col-sm-offset-5 col-sm-10">
        <a href="{{URL::to('/employee')}}" class="btn btn-danger">Back</a>
      </div>
    </div>    
</div>
</body>
</html>